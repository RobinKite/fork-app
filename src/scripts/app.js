const TEXT = '-pressed';
const githubButtons =
	document.querySelector('.about__list');

const burgerMenu = document.querySelector('.burger-menu');
const navList = document.querySelector('.nav__list');

githubButtons.addEventListener('click', (event) => {
	if (!event.target.closest('.github__button')) return;

	const target = event.target.closest('.github__button');
	const useTag = target.querySelector('use');
	const href = useTag.getAttribute('href');
	const counter = target.nextElementSibling;
	const num = parseInt(
		counter.textContent.replace(',', '')
	);

	useTag.attributes.href.nodeValue = href.includes(TEXT)
		? href.replace(TEXT, '')
		: href + TEXT;
	counter.textContent = href.includes(TEXT)
		? numberWithCommas(num - 1)
		: numberWithCommas(num + 1);
});

document.addEventListener('click', (e) => {
	if (e.target.closest('.burger-menu') == burgerMenu) {
		burgerMenu.classList.toggle('burger-menu--active');
		navList.classList.toggle('nav__list--active');
	} else if (
		navList.classList.contains('nav__list--active')
	) {
		burgerMenu.classList.toggle('burger-menu--active');
		navList.classList.toggle('nav__list--active');
	}
});

function numberWithCommas(x) {
	return x.toString().replace(/(?=(\d{3})+(?!\d))/g, ',');
}
