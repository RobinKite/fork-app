# Forkio

You can find our project [here](https://robinkite.gitlab.io/fork-app)

Fork App is a project created by Alex and Bohdan.

## Technologies

The following technologies were used in this project:

-  Webpack
-  Babel
-  HTML
-  SCSS
-  Javascript

## Contributors

-  Alex Bird 🦅
-  Bohdan 💫

## Task Distribution

-  Alex Bird:

   -  Sections: header, hero, testimonials.
   -  Webpack build tool
   -  Js code

-  Bohdan:

   -  Sections: about, features, pricing.
   -  Mixins, functions
   -  Media queries
