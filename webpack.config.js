const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const {
	CleanWebpackPlugin,
} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const isDev = process.env.NODE_ENV === 'development';
const isProd = !isDev;

const filename = (ext) =>
	isDev ? `[name].${ext}` : `[name].[contenthash].${ext}`;

module.exports = {
	context: path.resolve(__dirname, 'src'),
	mode: 'development',
	entry: {
		main: path.join(__dirname, 'src', 'index.mjs'),
	},
	output: {
		clean: true,
		filename: `./scripts/scripts.min.js`,
		path: path.resolve(__dirname, 'dist'),
	},
	devServer: {
		historyApiFallback: true,
		static: {
			directory: path.join(__dirname, 'dist'),
		},
		open: true,
		compress: true,
		hot: true,
		port: 3000,
	},
	optimization: {
		minimize: true,
		minimizer: [
			new CssMinimizerPlugin(),
			new TerserPlugin(),
		],
	},
	plugins: [
		new HTMLWebpackPlugin({
			template: path.resolve(__dirname, './index.html'),
			filename: 'index.html',
			inject: 'body',
			minify: {
				collapseWhitespace: isProd,
			},
			hash: true,
		}),
		new CleanWebpackPlugin(),
		new MiniCssExtractPlugin({
			filename: `./styles/${filename('css')}`,
		}),
	],
	devtool: isProd ? false : 'source-map',
	module: {
		rules: [
			{
				test: /\.html$/i,
				loader: 'html-loader',
			},
			{
				test: /\.css$/i,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: { hmr: isDev },
					},
					'css-loader',
				],
			},
			{
				test: /\.s[ac]ss$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							publicPath: (
								resourcePath,
								context
							) => {
								return (
									path.relative(
										path.dirname(resourcePath),
										context
									) + '/'
								);
							},
						},
					},
					'css-loader',
					'sass-loader',
				],
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: ['babel-loader'],
			},
			{
				test: /\.(png|jpe?g|gif|webp|ico|svg)$/i,
				type: 'asset/resource',
				generator: {
					filename: 'images/[name][contenthash][ext]',
				},
			},
			{
				test: /\.(woff|woff2|eot|ttf|otf)$/i,
				type: 'asset/resource',
				generator: {
					filename: 'fonts/[name][contenthash][ext]',
				},
			},
		],
	},
};
